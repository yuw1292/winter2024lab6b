import java.util.Random;

public class Deck {
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck() {
		this.rng = new Random();
		this.cards = new Card[52];
		this.numberOfCards = 52;
		
		String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
        String[] values = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
        int index = 0;
		for (int isuit = 0; isuit < suits.length; isuit++) {
            for (int ivalue = 0; ivalue < values.length; ivalue++) {
                this.cards[index] = new Card(values[ivalue], suits[isuit]);
				index++;
            }
        }
    }
	
	public int length() {
		return numberOfCards;
	}
	
	public Card drawTopCard() {
		if (numberOfCards > 0) {
            return this.cards[--numberOfCards];
        } else {
			return null;
		}
	}
	
	public String toString() {
		if (numberOfCards == 0) {
        return "The deck is empty";
		}
		String deckString = "";
		for (int i = 0; i < numberOfCards; i++) {
			deckString += this.cards[i].toString();
			if (i < numberOfCards - 1) { // to make sure comma is not added after last card
				deckString += ", ";
			}
        }
		return deckString;
	}
	
	public void shuffle() {
		for (int i = 0; i < numberOfCards; i++) {
			int randomIndex = i + rng.nextInt(numberOfCards - i);
			Card temp = this.cards[i]; //puts card in a temporary slot
			this.cards[i] = this.cards[randomIndex]; //switches the two spots
			this.cards[randomIndex] = temp;
        }
    }
}


	