import java.util.Scanner;

public class LuckyCardGameApp {
	public static void main(String[] args) {
		
		GameManager manager = new GameManager();
		int totalPoints = 0;
		int round = 0;
		
		System.out.println("Welcome to the Lucky! card game");
		
		while (manager.getNumberOfCards() > 1 && totalPoints < 5) {
			round++;
			System.out.println("Round: " + round);
			System.out.println(manager.toString());
			
			totalPoints += manager.calculatePoints();
			System.out.println("Your points after round " + round + ": " + totalPoints);
			
			manager.dealCards();
			System.out.println("**************************************************************");
		}
		
		System.out.println("Final Score: " + totalPoints);
        if (totalPoints >= 5) {
            System.out.println("Congrats you win!");
        } else {
            System.out.println("Womp womp, you lose!");
        }
	}
}
	