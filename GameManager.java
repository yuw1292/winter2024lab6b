public class GameManager {
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager() {
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	public String toString() {
        return "-------------------------------------------------------------\n" +
               "Center card: " + this.centerCard + "\n" +
               "Player card: " + this.playerCard + "\n" +
               "-------------------------------------------------------------";
    }
	
	public void dealCards() {
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.drawPile.shuffle();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	public int getNumberOfCards() {
		return this.drawPile.length();
	}
	
	public int calculatePoints() {
		String centerCardValue = this.centerCard.getValue();
		String centerCardSuit = this.centerCard.getSuit();
		
		String playerCardValue = this.playerCard.getValue();
		String playerCardSuit = this.playerCard.getSuit();
		
		if(centerCardValue == playerCardValue) {
			return 4;
		}
		if(centerCardSuit == playerCardSuit) {
			return 2;
		}
		return -1;
	}
}